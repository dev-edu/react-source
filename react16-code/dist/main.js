/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
var __webpack_exports__ = {};

;// CONCATENATED MODULE: ./react/createElement.js
// const element = React.createElement('h1', { title: 'foo' }, 'Hello');

function createElement(type, props) {
  for (var _len = arguments.length, children = new Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
    children[_key - 2] = arguments[_key];
  }
  return {
    type,
    props: {
      ...props,
      children: children.map(child => typeof child === 'object' ? child : createTextElement(child))
    }
  };
}
function createTextElement(text) {
  return {
    type: 'TEXT_ELEMENT',
    props: {
      nodeValue: text,
      children: []
    }
  };
}
;// CONCATENATED MODULE: ./react/react-dom.js
// 下一个功能单元
let nextUnitOfWork = null;
function render(element, container) {
  // console.log('element: ', element);
  //memo 是不是要整个fiber tree进行优化 {}

  //将我们的根节点设置成为第一个工作单位
  nextUnitOfWork = {
    dom: container,
    props: {
      children: [element]
    }
  };
}
function createDom(fiber) {
  const dom = fiber.type == 'TEXT_ELEMENT' ? document.createTextNode('') : document.createElement(fiber.type);
  const isProperty = key => key !== 'children';
  Object.keys(fiber.props).filter(isProperty).forEach(name => {
    dom[name] = fiber.props[name];
  });
  return dom;
  // stack 同步的 不能被中断的
  // element.props.children.forEach((child) => render(child, dom));
  // container.appendChild(dom);

  //---为什么要换掉原来的 不能中断的新的心智模型---
  // while (下一个工作单元) {
  //   下一个工作单元 = 执行工作单元（下一个工作单元丢进去）
  // }
}

/*
 *工作循环
 *@param {*} deadline 截止时间
 */
function workLoop(deadline) {
  //停止停止标识
  let shouldYield = false;
  while (nextUnitOfWork && !shouldYield) {
    //执行工作单元
    nextUnitOfWork = performUnitOfWork(nextUnitOfWork);
    //判断是否需要停止
    //模拟的的情况是咱们自己还得判断下有没有16.67
    shouldYield = deadline.timeRemaining() < 1;
  }
}

// 空闲时间执行任务
requestIdleCallback(workLoop);

// 执行单元事件，返回下一个单元事件
function performUnitOfWork(fiber) {
  // 如果fiber上没有dom节点，为其创建一个
  if (!fiber.dom) {
    fiber.dom = createDom(fiber);
  }
  console.log('fiber: ', fiber);
  // 如果fiber有父节点，将fiber.dom添加到父节点
  if (fiber.parent) {
    fiber.parent.dom.appendChild(fiber.dom);
  }

  // 获取到当前fiber的孩子节点
  const elements = fiber.props.children;
  // 索引
  let index = 0;
  // 上一个兄弟节点
  let prevSibling = null;

  // 遍历孩子节点
  while (index < elements.length) {
    const element = elements[index];
    // 创建fiber
    const newFiber = {
      type: element.type,
      props: element.props,
      parent: fiber,
      dom: null
    };

    // 将第一个孩子节点设置为 fiber 的子节点
    if (index === 0) {
      fiber.child = newFiber;
    } else if (element) {
      // 第一个之外的子节点设置为第一个子节点的兄弟节点
      prevSibling.sibling = newFiber;
    }
    prevSibling = newFiber;
    index++;
  }

  // 寻找下一个孩子节点，如果有返回
  if (fiber.child) {
    return fiber.child;
  }
  let nextFiber = fiber;
  while (nextFiber) {
    // 如果有兄弟节点，返回兄弟节点
    if (nextFiber.sibling) {
      return nextFiber.sibling;
    }
    // 否则返回父节点
    nextFiber = nextFiber.parent;
  }
}
;// CONCATENATED MODULE: ./react/index.js


const React = {
  createElement: createElement,
  render: render
};
/* harmony default export */ const react = (React);
;// CONCATENATED MODULE: ./src/index.js
// import React from 'react';


const src_element = /*#__PURE__*/react.createElement("div", null, /*#__PURE__*/react.createElement("h1", {
  title: "foo"
}, /*#__PURE__*/react.createElement("span", null, "Hello")), /*#__PURE__*/react.createElement("a", {
  href: ""
}, "\u6D4B\u8BD5\u94FE\u63A5"));
console.log('element: ', src_element);

// const node = document.createElement(element.type);
// node['title'] = element.props.title;

// const text = document.createTextNode('');
// text['nodeValue'] = element.props.children;

// node.appendChild(text);

const container = document.getElementById('root');
// container.appendChild(node);
react.render(src_element, container);
/******/ })()
;
